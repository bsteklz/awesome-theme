#!/bin/bash

SRC_DIR=$(cd $(dirname $0) && pwd)
ROOT_UID=0


echo Font and Theme
#eger kullanici root ise normal kullanicia gecmesini soyleyip kapan

#awesome.sh theme/ wallpaper/ font/
#$HOME/.themes 
#$HOME/.fonts

# Destination directory
  THEME_DIR="$HOME/.themes"  
  FONT_DIR="$HOME/.fonts"
  WALLPAPER_DIR="$HOME/.wallpaper"

THEME_NAME=Whitesur-light
FONT_NAME=San-Francisco 
WALLPAPER_NAME=Whitesur

[[ ! -d ${THEME_DIR} ]] && mkdir -p ${THEME_DIR}
[[ ! -d ${FONT_DIR} ]] && mkdir -p ${FONT_DIR}
[[ ! -d ${WALLPAPER_DIR} ]] && mkdir -p ${WALLPAPER_DIR}


install() {
  local name=${1}
  tar -xvf theme.tgz -C ${THEME_DIR}./theme/ 
#  cp -r ${SRC_DIR}/themes/*                     ${THEME_DIR}
  cp -r ${SRC_DIR}./font/                        ${FONT_DIR}
  cp -r ${SRC_DIR}/wallpapers/                   ${WALLPAPER_DIR}


  echo "Installing '${THEME_NAME} kde themes'..."

  echo "Installing '${FONT_NAME} fonts'..."

  echo "Installing  '${WALLPAPER_NAME}  wallpaper' ..."


 install "${name:-${THEME_NAME}}"
 install "${name:-${FONT_NAME}}"
 install "${name:-${WALLPAPER_NAME}}

 echo "Install finished..."
